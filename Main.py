# coding: utf-8
#This is the main file. Run this, to run the program

import random
import pygame
from pygame.locals import *
import math
import sys

import Map
import Menu

#Setup
pygame.init()
pygame.font.init()

#colors
black 	= (0	,0		,0)
red		= (255	,0		,0)
green	= (0	,255	,0)
yellow	= (255	,255	,0)
blue	= (0	,0		,255)
pink	= (255	,0		,255)
cyan	= (0	,255	,255)
white 	= (255	,255	,255)


	
# set up fonts
basicFont = pygame.font.SysFont(None, 48)

FPS = 600 # 60 frames per second
clock = pygame.time.Clock()


effects = []
done = False
font = pygame.font.SysFont("Calibri", 15)

#window
SWIDTH =  800
SHEIGTH = 500
global MainWindow
MainWindow = pygame.display.set_mode((SWIDTH, SHEIGTH))
result = 'start'


def menu():
	global done
	done = False
	buttons = []
	buttons.append(Menu.Sbutton(250,120,200,100))
	buttons.append(Menu.Obutton(250,320,200,100))
	while not done:
		for event in pygame.event.get(): # User did something
			if event.type == pygame.QUIT: # Closed from X
				done = True # Stop the Loop
				result = None
			if event.type == pygame.MOUSEBUTTONUP:
				for button in buttons:
					result=button.pressed(pygame.mouse.get_pos())
					if not result == False and not result == None:
						done = True 
		if not done:
			MainWindow.fill(white)
			for button in buttons:
				button.draw()
			pygame.display.flip()
			clock.tick(FPS)
			pygame.display.set_caption("FPS: %i" % clock.get_fps())
			

def game():
	global DT 
	global done
	global nations
	global map
	done = False
	DT = 0
	nations = []
	map = Map.Europe()
	for tile in map:
		tile.set_tile()
	mapmode = 'political'
	buttons = []
	buttons.append(Menu.Mbutton(0,400,200,100))
	buttons.append(Menu.Polmod(250,400,50,50))
	buttons.append(Menu.Cormod(300,400,50,50))

	while not done:
		for event in pygame.event.get(): # User did something
			if event.type == pygame.QUIT: # Closed from X
				done = True # Stop the Loop				
				result = None
			if event.type == pygame.MOUSEBUTTONUP:
				for button in buttons:
					result=button.pressed(pygame.mouse.get_pos())
					if isinstance(result, basestring):
						mapmode = result
					elif not result == False and not result == None:
						result()
						done = True
						
					 
		if not done:
			MainWindow.fill(white)
			for nation in nations:
				nation.step(mapmode)
				if nation.in_exile:
					label = font.render('EX' + nation.name + ' ' + str(round(nation.pop/1000,0)) + 'K    ' + str(round(nation.ratio,3)), 1, red)
				else:
					label = font.render(nation.name + ' ' + str(round(nation.pop/1000,0)) + 'K    ' + str(round(nation.ratio,3)), 1, nation.colour)
				MainWindow.blit(label, (500,15*nation.num))
			for tile in map:
				if tile.occupied > 0:
					tile.draw(mapmode)
			for button in buttons:
				button.draw()
			#label = font.render('Total death toll: ' + str(DT) +'k', 3, red)
			pygame.display.flip()
			clock.tick(FPS)
			pygame.display.set_caption("FPS: %i" % clock.get_fps())
			
if not result == None and not result == 'start':
	result()
if result == 'start':
	menu()
pygame.quit()