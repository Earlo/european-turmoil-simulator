import pygame
import random

import Nations
black 	= [0	,0		,0]
red		= [255	,0		,0]
dred	= [155	,0		,0]
green	= [0	,255	,0]
dgreen	= [0	,155	,0]
yellow	= [255	,255	,0]
blue	= [0	,0		,255]
dblue 	= [0	,0		,155]
pink	= [255	,0		,255]
dpink	= [255	,0		,155]
cyan	= [0	,255	,255]
white 	= [255	,255	,255]
gray 	= [128	,128	,128]
lbrown	= [200	,200	,100]
dorange = [255	,140	,0]


class Tile (object):
	def __init__(self,x,y,ocp):
		self.ruler = None
		self.pos = [x,y]
		self.width = [x*10,y*10]
		self.heigth = [10,10]
		self.occupied = ocp
		if self.occupied == 1:
			self.colour = black
		elif self.occupied == 0:
			self.colour = white
		elif self.occupied == 2:
			Nations.Country(self,'ENG','The Kingdom of Cocroachia',red,self.occupied)
		elif self.occupied == 3:
			Nations.Country(self,'FRA','Frog Rebublic',dblue,self.occupied)
		elif self.occupied == 4:
			Nations.Country(self,'GER','Sayrkrautreich',gray,self.occupied)
			self.ruler.pop += 50
		elif self.occupied == 5:
			Nations.Country(self,'SWE','Great Caliphate of The North',blue,self.occupied)			
		elif self.occupied == 6:
			Nations.Country(self,'ITA','Kleptocracy',green,self.occupied)	
		elif self.occupied == 7:
			Nations.Country(self,'TUR','Kebabstania',dred,self.occupied)				
		elif self.occupied == 8:
			Nations.Country(self,'RUS','Slaviet Union',dgreen,self.occupied)
			self.ruler.pop -= 10
		elif self.occupied == 9:
			Nations.Country(self,'SPA','Siestastanias',yellow,self.occupied)
		elif self.occupied == 10:
			Nations.Country(self,'JEW','Union Of Happy Merchants',cyan,self.occupied)
			self.ruler.pop -= 200
		elif self.occupied == 11:
			Nations.Country(self,'FIN','The Northern Mongolhorde',lbrown,self.occupied)
			self.ruler.pop -= 50
		elif self.occupied == 12:
			Nations.Country(self,'NED','Smoke Weed Everyday nation',dorange,self.occupied)
			self.ruler.pop -= 10
		elif self.occupied == 13:
			Nations.Country(self,'POL','Poland',dpink,self.occupied)
			self.ruler.pop -= 50
			
		self.line = 1
		self.movcost = 1
		self.fscore = 1 # h score(estimated cost)
		self.next = []	# nodes next to this one go in heres
		
	def set_tile(self):
		from Main import map
		self.upleft = 0
		self.up = 0
		self.upright = 0
		self.left = 0
		self.right = 0
		self.downleft = 0
		self.down = 0
		self.downright = 0

		for n in range(8):
			self.next.append(None)
		self.parent = None
		for tile in map:
			if tile.pos[0] == self.pos[0] - 1:
				if tile.pos[1] == self.pos[1] - 1:		# 4 1 5
					self.next[4] = tile  #upleft		# 0 x 2
				elif tile.pos[1] == self.pos[1]:		# 7 3 6
					self.next[1] = tile  #up
				elif tile.pos[1] == self.pos[1] + 1:
					self.next[5] = tile  #uprigth
			elif tile.pos[0] == self.pos[0]:
				if tile.pos[1] == self.pos[1] - 1:
					self.next[0] = tile  #left
				elif tile.pos[1] == self.pos[1] + 1:
					self.next[2] = tile  #rigth
			elif tile.pos[0] == self.pos[0] + 1:
				if tile.pos[1] == self.pos[1] - 1:
					self.next[7] = tile  #downleft		
				elif tile.pos[1] == self.pos[1]:
					self.parent = tile  #down
				elif tile.pos[1] == self.pos[1] + 1:
					self.next[6] = tile #downrigth
	def draw(self,mode):
		from Main import MainWindow
		if self.ruler == None:
			pygame.draw.rect(MainWindow,black,(self.width,self.heigth),self.line)
		else:
			if mode == 'political':
				pygame.draw.rect(MainWindow,self.ruler.colour,(self.width,self.heigth),self.line)
			elif mode == 'cores':
				pygame.draw.rect(MainWindow,self.ruler.colour,(self.width,self.heigth),1)
			
#~~~~~~~~~~~~~~~~~~~~~~~~~Tile end~~~~~~~~~~~~~~~~~~~~~
def build_map(blueprint,size):
	map = [Tile(x,y,blueprint[size[0]*y+x]) for x in range(size[0]) for y in range(size[1])]
	return map
	
def Europe():
	size = [46,36]
	blueprint =[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
				0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,1,1,1,0,1,1,1,1,5,0,0,0,1,11,1,1,1,1,0,1,1,1,1,1,1,1,1,
				0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,1,0,0,0,1,1,1,0,0,0,0,0,0,0,0,1,0,0,1,1,1,1,1,1,1,1,
				0,0,0,0,0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1,1,1,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
				0,0,0,0,0,1,1,0,1,1,1,0,0,0,0,0,0,0,0,0,1,1,1,0,0,1,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
				0,0,0,0,1,1,1,0,0,1,1,1,0,0,0,0,0,0,0,0,1,1,1,1,1,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,8,1,
				0,0,0,1,1,1,1,0,0,1,1,1,1,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
				0,0,0,1,1,1,0,0,1,1,1,1,1,1,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
				0,0,0,0,1,0,0,0,1,1,1,1,2,0,0,0,12,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
				0,0,0,0,0,0,0,0,0,0,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
				0,0,0,0,0,0,0,1,1,1,1,0,0,0,1,1,1,1,1,1,1,1,1,1,4,1,1,1,1,1,1,13,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
				0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
				0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
				0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
				0,0,0,0,0,0,0,0,0,1,1,1,1,1,3,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
				0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,1,0,0,0,1,1,1,1,
				0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,1,1,1,0,1,1,1,1,1,
				0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,1,1,1,
				0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,0,0,0,0,1,1,0,0,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,1,1,1,1,
				0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,0,0,0,0,0,0,0,1,1,0,0,1,1,1,1,1,1,1,0,0,0,0,0,0,1,1,1,1,1,1,
				0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,1,1,1,0,1,1,1,1,1,0,0,7,1,1,1,1,1,1,1,1,1,1,1,
				0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,1,1,0,1,1,1,1,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,
				0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,6,1,0,1,1,1,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,
				0,0,0,0,0,0,1,1,1,9,1,1,1,1,0,0,0,0,0,0,0,0,0,0,1,1,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,
				0,0,0,0,0,0,1,1,1,1,1,1,1,1,0,0,0,0,0,0,1,1,1,1,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,
				0,0,0,0,0,0,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,10,1,1,1,1,1,
				0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,
				0,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,
				0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,1,1,1,1,1,1,1,
				0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
	return build_map(blueprint,size)
