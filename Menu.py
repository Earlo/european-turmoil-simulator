# -*- coding: cp1252 -*-
import pygame, sys
from pygame.locals import *

black 	= (0	,0		,0)
red		= (255	,0		,0)
green	= (0	,255	,0)
yellow	= (255	,255	,0)
blue	= (0	,0		,255)
pink	= (255	,0		,255)
cyan	= (0	,255	,255)
white 	= (255	,255	,255)
pygame.font.init()

class Polmod (object):
	def __init__(self,x,y,w,l):
		self.pos =(x,y)
		self.width = w
		self.length = l
		self.text = "Political"
		self.font = pygame.font.SysFont("Calibri", 15)
		self.label = self.font.render(self.text, 1, black)
	def pressed(self, mouse):
		if mouse[0] > self.pos[0]:
			if mouse[1] > self.pos[1]:
				if mouse[0] < self.pos[0] + self.width:
					if mouse[1] < self.pos[1] + self.length:
						return 'political'
					else: return False
				else: return False
			else: return False
		else: return False
	
	def draw(self):
		from Main import MainWindow
		pygame.draw.rect(MainWindow, blue, (self.pos[0],self.pos[1],self.width,self.length),0)
		MainWindow.blit(self.label, (self.pos[0],self.pos[1]))
class Cormod (object):
	def __init__(self,x,y,w,l):
		self.pos =(x,y)
		self.width = w
		self.length = l
		self.text = "Cores"
		self.font = pygame.font.SysFont("Calibri", 15)
		self.label = self.font.render(self.text, 1, black)
	def pressed(self, mouse):
		if mouse[0] > self.pos[0]:
			if mouse[1] > self.pos[1]:
				if mouse[0] < self.pos[0] + self.width:
					if mouse[1] < self.pos[1] + self.length:
						return 'cores'
					else: return False
				else: return False
			else: return False
		else: return False
	
	def draw(self):
		from Main import MainWindow
		pygame.draw.rect(MainWindow, blue, (self.pos[0],self.pos[1],self.width,self.length),0)
		MainWindow.blit(self.label, (self.pos[0],self.pos[1]))
		
class Mbutton (object):
	def __init__(self,x,y,w,l):
		self.pos =(x,y)
		self.width = w
		self.length = l
		self.text = "Menu"
		self.font = pygame.font.SysFont("Calibri", 15)
		self.label = self.font.render(self.text, 1, black)
	def pressed(self, mouse):
		if mouse[0] > self.pos[0]:
			if mouse[1] > self.pos[1]:
				if mouse[0] < self.pos[0] + self.width:
					if mouse[1] < self.pos[1] + self.length:
						from Main import menu
						return menu()
					else: return False
				else: return False
			else: return False
		else: return False
	
	def draw(self):
		from Main import MainWindow
		pygame.draw.rect(MainWindow, blue, (self.pos[0],self.pos[1],self.width,self.length),0)
		MainWindow.blit(self.label, (self.pos[0],self.pos[1]))
		
class Sbutton (object):
	def __init__(self,x,y,w,l):
		self.pos =(x,y)
		self.width = w
		self.length = l
		self.text = "Start!"
		self.font = pygame.font.SysFont("Calibri", 15)
		self.label = self.font.render(self.text, 1, black)
	def pressed(self, mouse):
		if mouse[0] > self.pos[0]:
			if mouse[1] > self.pos[1]:
				if mouse[0] < self.pos[0] + self.width:
					if mouse[1] < self.pos[1] + self.length:
						from Main import game
						return game()
					else: return False
				else: return False
			else: return False
		else: return False
	
	def draw(self):
		from Main import MainWindow
		pygame.draw.rect(MainWindow, blue, (self.pos[0],self.pos[1],self.width,self.length),0)
		MainWindow.blit(self.label, (self.pos[0],self.pos[1]))
		
class Obutton (object):
	def __init__(self,x,y,w,l):
		self.pos =(x,y)
		self.width = w
		self.length = l
		self.text = "Options"
		self.font = pygame.font.SysFont("Calibri", 15)
		self.label = self.font.render(self.text, 1, black)
		
	def pressed(self, mouse):
		if mouse[0] > self.pos[0]:
			if mouse[1] > self.pos[1]:
				if mouse[0] < self.pos[0] + self.width:
					if mouse[1] < self.pos[1] + self.length:
						return True
					else: return False
				else: return False
			else: return False
		else: return False
	
	def draw(self):
		from Main import MainWindow
		pygame.draw.rect(MainWindow, blue, (self.pos[0],self.pos[1],self.width,self.length),0)
		MainWindow.blit(self.label, (self.pos))