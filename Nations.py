

import random
import pygame

pygame.init()
black 	= [0	,0		,0]
ENG		= [255	,0		,0]
green	= (0	,255	,0)
yellow	= (255	,255	,0)
blue	= (0	,0		,255)
pink	= (255	,0		,255)
cyan	= (0	,255	,255)
white 	= [255	,255	,255]
def Possible_peace(ruler1,ruler2):
	if random.randint(0,100) +random.randint(0,100) == 200:
		ruler1.enemies.remove(ruler2)
		ruler2.enemies.remove(ruler1)
#		print self.name + ' declared war on ' + target.ruler.name + '.'
		return True
	else:
		return False
def chaholder(tile,receiver):
	tile.ruler.realm.remove(tile)
	receiver.realm.append(tile)	
	tile.occupied = receiver.num
	tile.ruler = receiver
		
def battle(atta,defe,are,dre):
	if defe.pop <= 0:
		defe.pop = 1
	if atta.pop <= 0:
		atta.pop += 1
	a_mp = atta.pop/are
	d_mp = defe.pop/dre
	a_wp = atta.res/float(atta.pop)
	atta.ratio = a_wp
	d_wp = defe.res/float(defe.pop)
	defe.ratio = d_wp
	aro = 1+(random.randint(0,50) + random.randint(0,50))/100.
	dro = 1.25+(random.randint(0,50) + random.randint(0,50))/100.
	at = a_mp*a_wp*aro
	de = d_mp*d_wp*dro
	if at > de:
		atta.pop -= a_mp/8
		atta.res -= a_wp
		defe.pop -= d_mp/8
		defe.res -= d_wp
		return True
	else:
		atta.pop -= a_mp/5
		atta.res -= a_wp
		defe.pop -= d_mp/12
		defe.res -= d_wp
		return False
		
class Country (object):
	def __init__(self,tile,id,name,colour,num):
		self.capitol = tile
		self.in_exile = False
		self.cores = []
		self.cores.append(tile)
		self.ratio = 0
		self.name = name
		self.ID = id
		self.num = num
		self.colour = colour
		tile.colour = colour
		self.realm =[]
		self.realm.append(tile)
		self.pop = 10
		self.res = 0
		self.enemies = []
		from Main import nations
		nations.append(self)
		tile.ruler = self
	
	def step(self,mode):
		if not self.in_exile:
			for tile in self.realm:
				if tile in self.cores:
					self.res += .5
					self.pop += 10
				else:
					self.res += 0.1
					self.pop += 4
				n = random.randint(0,7)
				if not tile.next[n] == None:
					target = tile.next[n]
					if target.occupied == 1:
						if self.pop/len(self.realm) > 10:
							self.realm.append(target)
							self.cores.append(target)
							target.colour = self.colour
							target.occupied = self.num
							target.ruler = self
							target.line = 0
							self.pop -=10
					elif target.occupied > 1 and not target.ruler == self:
						if not target.ruler in self.enemies:
							if random.randint(0,100) +random.randint(0,100) == 200:
								self.enemies.append(target.ruler)
								target.ruler.enemies.append(self)
#								print self.name + ' declared war on ' + target.ruler.name + '.'
						elif not Possible_peace(self,target.ruler):
							if battle(self,target.ruler,len(self.realm),len(target.ruler.realm)):
								if len(target.ruler.realm) == 1:
#										print target.ruler.name + ' Has been wiped out from the face of the Earth.'
									target.ruler.in_exile = True
									for enemy in target.ruler.enemies:
										enemy.enemies.remove(target.ruler)
									target.ruler.enemies = []
								chaholder(target,self)
		else:
			if random.randint(0,50) + random.randint(0,50) == 100:
#				print 'Loyal supporters of ' + self.name + ' have risen to rebellion against their opressors.'
				for target in self.cores:
					self.res += .5
					self.pop += 5
					if  not target.ruler in self.enemies:
						self.enemies.append(target.ruler)
						target.ruler.enemies.append(self)
					if len(target.ruler.realm) == 1:
						target.ruler.in_exile = True
					if not battle(target.ruler,self,len(target.ruler.realm),len(self.cores)):
						chaholder(target,self)
				if len(self.realm) > 0:
					self.in_exile = False
			else:
				for tile in self.cores:
					self.res += 0.4
					self.pop += 6
		if mode == 'cores':
			for tile in self.cores:
				from Main import MainWindow
				pygame.draw.circle( MainWindow, self.colour, (tile.width[0] + 5,tile.width[1] + 5), 4, 0)
